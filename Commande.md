# Commande


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                       |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------         |
| /commandes            	   | GET         | <-application/json<br><-application/xml                      |                 | La liste des commandes                                         |
| /commandes/{id}       	   | GET         | <-application/json<br><-application/xml                      |                 | Une commande                                        |
| /commandes/{id}/name   	   | GET         | <-text/plain                                                 |                 | Le nom d'une commande                                    |
| /commandes             	   | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Crée une nouvelle commande |
| /commandes/{id}        	   | DELETE      | 												                |                 | Supprime une commande                                            |   
 
On peut créer une commande avec ou sans pizzas, la supprimer ou l'afficher.
Afficher la liste des commandes ou afficher le nom d'une commande.