# Pizza


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                       |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------         |
| /pizzas            	   | GET         | <-application/json<br><-application/xml                      |                 | La liste des pizzas                                         |
| /pizzas/{id}       	   | GET         | <-application/json<br><-application/xml                      |                 | Une pizza                                        |
| /pizzas/{id}/name   	   | GET         | <-text/plain                                                 |                 | Le nom d'une pizza                                    |
| /pizzas             	   | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Crée une nouvelle pizza |
| /pizzas/{id}        	   | DELETE      | 												                |                 | Supprime une pizza                                            |   
 
On peut créer une pizza avec ou sans ingrédients, la supprimer ou l'afficher.
Afficher la liste des pizzas ou afficher le nom d'une pizza.