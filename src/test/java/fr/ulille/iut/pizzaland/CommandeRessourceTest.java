package fr.ulille.iut.pizzaland;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeRessourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(CommandeRessourceTest.class.getName());
	private PizzaDao dao;
	private IngredientDao daoI;
	private CommandeDao daoC;


	 @Override
	    protected Application configure() {
	    	BDDFactory.setJdbiForTests();
	        return new ApiV1();
	    }

	    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	    // base de données
	    // et les DAO

	    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	    @Before
	    public void setEnvUp() {
	    	dao = BDDFactory.buildDao(PizzaDao.class);
			dao.createTableAndIngredientAssociation();
			daoI = BDDFactory.buildDao(IngredientDao.class);
			daoI.createTable();
			daoC = BDDFactory.buildDao(CommandeDao.class);
			daoC.createTableAndPizzaAssociation();
	    }

	    @After
	    public void tearEnvDown() throws Exception {
	       dao.dropAssiociationTable();
	       daoI.dropTable();
	       daoC.dropTableAndPizzaAssociation();
	    }
	    
	    @Test
		public void testGetExistingCommandeWithoutPizzas() {
			Commande commande = new Commande();
			commande.setName("Commande");
			daoC.insertCommande(commande);

			Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

			assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

			Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
			assertEquals(commande, result);
		}

		@Test
		public void testGetNotExistingCommande() {
			Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
		}

		@Test
		public void testCreateCommandeWithoutPizzas() {
			CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
			commandeCreateDto.setName("Commande");

			Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

			assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

			CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

			assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
			assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
		}

		@Test
		public void testCreateSameCommande() {
			Commande commande = new Commande();
			commande.setName("Commande");
			daoC.insertCommande(commande);

			CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
			Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

			assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
		}

		@Test
		public void testCreateCommandeWithoutName() {
			CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

			Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

			assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
		}

		@Test
		public void testDeleteExistingCommande() {
			Commande commande = new Commande();
			commande.setName("Commande");
			daoC.insertCommande(commande);

			Response response = target("/commandes/").path(commande.getId().toString()).request().delete();

			assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

			Commande result = daoC.findById(commande.getId());
			assertEquals(result, null);
		}

		@Test
		public void testDeleteNotExistingCommande() {
			Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
		}

		@Test
		public void testGetCommandeName() {
			Commande commande = new Commande();
			commande.setName("Commande");
			daoC.insertCommande(commande);

			Response response = target("commandes").path(commande.getId().toString()).path("name").request().get();

			assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

			assertEquals("Commande", response.readEntity(String.class));
		}


		@Test
		public void testGetExistingCommandeWithPizza() {
			ArrayList<Pizza> l = new ArrayList();
			Pizza i = new Pizza("Chorizo");
			l.add(i);
			Commande commande = new Commande("commande", l);

			dao.insertPizza(i);
			daoC.insertTableCommandeAndPizzaAssociation(commande);

			Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

			assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

			Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
			assertEquals(result.getName(),commande.getName());
		}


		@Test
		public void testCreateCommandeWithPizzas() {
			ArrayList<Pizza> l = new ArrayList();
			Pizza i = new Pizza("Chorizo");
			l.add(i);
			Commande commande = new Commande("commande", l);

			dao.insertPizza(i);
			CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);

			Response response = target("commandes").request().post(Entity.json(commandeCreateDto));

			assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

			CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

			assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
			assertEquals(returnedEntity.getName(), commandeCreateDto.getName());

		}
}
