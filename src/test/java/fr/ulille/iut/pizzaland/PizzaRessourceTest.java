package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaRessourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessourceTest.class.getName());
	private PizzaDao dao;
	private IngredientDao daoI;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTableAndIngredientAssociation();
		daoI = BDDFactory.buildDao(IngredientDao.class);
		daoI.createTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndIngredientAssociation();
		daoI.dropTable();
	}

	@Test
	public void testGetExistingPizzaWithoutIngredients() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		dao.insertPizza(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutIngredients() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Chorizo");

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}

	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		dao.insertPizza(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		dao.insertPizza(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		dao.insertPizza(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Chorizo", response.readEntity(String.class));
	}


	@Test
	public void testGetExistingPizzaWithIngredient() {
		ArrayList<Ingredient> l = new ArrayList();
		Ingredient i = new Ingredient("tomate");
		l.add(i);
		Pizza pizza = new Pizza("margherita", l);

		daoI.insert(i);
		dao.insertTablePizzaAndIngredientAssociation(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(result.getName(),pizza.getName());
	}


	@Test
	public void testCreatePizzaWithIngredients() {
		ArrayList<Ingredient> l = new ArrayList();
		Ingredient i = new Ingredient("tomate");
		l.add(i);
		Pizza pizza = new Pizza("margherita", l);

		daoI.insert(i);
		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);

		Response response = target("pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());

	}

}
