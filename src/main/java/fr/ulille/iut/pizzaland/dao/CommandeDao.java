package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.Commande;

public interface CommandeDao {


	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commandes (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandePizzasAssociation (commandeID VARCHAR(128), pizzaID VARCHAR(128), PRIMARY KEY(commandeID, pizzaID))")
	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS Commandes")
	void dropTable();

	@SqlUpdate("DROP TABLE IF EXISTS CommandePizzasAssociation")
	void dropAssiociationTable();

	@SqlUpdate("INSERT INTO CommandePizzasAssociation (commandeID, pizzaID) VALUES (:commandeID, :pizzaID)")
	void insertInAssiociationTable(@Bind("commandeID") UUID commandeID,@Bind("pizzaID") UUID pizzaID);
	
	@SqlUpdate("DELETE FROM Commandes WHERE id = :id")
    void remove(@Bind("id") UUID id);

	@SqlUpdate("INSERT INTO Commandes (id, name) VALUES (:id, :name)")
	void insertCommande(@BindBean Commande commande);

	@SqlQuery("SELECT * FROM Commandes WHERE name = :name")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM Commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();

	@SqlQuery("SELECT * FROM Commandes WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(@Bind("id") UUID id);
	
	default void insertTableCommandeAndPizzaAssociation(Commande commande) {
    	this.insertCommande(commande);
    	for(Pizza pizza : commande.getPizzas()) {
    		this.insertInAssiociationTable(commande.getId(),pizza.getId());
    	}
    }


	@Transaction
	default void createTableAndPizzaAssociation() {
		createAssociationTable();
		createCommandeTable();
	}

	@Transaction
	default void dropTableAndPizzaAssociation() {
		dropAssiociationTable();
		dropTable();
	}
}
