package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	List<Ingredient> list;
		
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	public List<Ingredient> getList() {
		return list;
	}

	public void setList(List<Ingredient> list) {
		this.list = list;
	}
}
