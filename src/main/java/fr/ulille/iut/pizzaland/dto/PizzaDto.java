package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {

	private UUID id;
	private String name;
	private List<Ingredient> liste;

	public List<Ingredient> getListe() {
		return liste;
	}

	public void setListe(List<Ingredient> liste) {
		this.liste = liste;
	}

	public PizzaDto() {

	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
