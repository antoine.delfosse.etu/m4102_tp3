package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/commandes")
public class CommandeRessource {
	private static final Logger LOGGER = Logger.getLogger(CommandeRessource.class.getName());

	private CommandeDao commandes;

	@Context
	public UriInfo uriInfo;

	public CommandeRessource() {
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createCommandeTable();
	}

	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeRessource:getAll");

		List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande pizza = commandes.findById(id);
			LOGGER.info(pizza.toString());
			return Commande.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	public Response createCommande(CommandeCreateDto pizzaCreateDto) {
		Commande existing = commandes.findByName(pizzaCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Commande pizza = Commande.fromCommandeCreateDto(pizzaCreateDto);
			commandes.insertCommande(pizza);
			CommandeDto pizzaDto = Commande.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}

	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if ( commandes.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		commandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	@GET
	@Path("{id}/name")
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande pizza = commandes.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getName();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createCommande(@FormParam("name") String name) {
		Commande existing = commandes.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Commande pizza = new Commande();
			pizza.setName(name);

			commandes.insertCommande(pizza);

			CommandeDto pizzaDto = Commande.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
}
